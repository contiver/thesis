To compile the thesis.pdf file:
```
pdflatex thesis.tex
bibtex thesis
pdflatex thesis.tex
pdflatex thesis.tex
```

Or just run `./compile.sh`.
